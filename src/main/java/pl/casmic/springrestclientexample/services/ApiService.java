package pl.casmic.springrestclientexample.services;

import pl.casmic.api.domain.Customer;

import java.util.List;

public interface ApiService {

    List<Customer> getCustomers();

    Customer getCustomerByID(String id);

}
