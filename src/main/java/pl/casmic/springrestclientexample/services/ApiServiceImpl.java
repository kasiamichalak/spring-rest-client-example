package pl.casmic.springrestclientexample.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import pl.casmic.api.domain.Customer;
import pl.casmic.api.domain.Customers;

import java.util.List;

@Service
public class ApiServiceImpl implements ApiService {

    private RestTemplate restTemplate;

    private final String apiUrl;

    public ApiServiceImpl(RestTemplate restTemplate, @Value("${api.url}") String apiUrl) {
        this.restTemplate = restTemplate;
        this.apiUrl = apiUrl;
    }


    @Override
    public List<Customer> getCustomers() {
        Customers customers = restTemplate.getForObject(apiUrl, Customers.class);
        return customers.getCustomers();
    }

    @Override
    public Customer getCustomerByID(String id) {
        String uri = apiUrl + id;
//        UriComponentsBuilder uriBuilder = UriComponentsBuilder
//                .fromUriString(apiUrl)
//                .queryParam("id", id);
        return restTemplate.getForObject(uri, Customer.class);
    }
}
