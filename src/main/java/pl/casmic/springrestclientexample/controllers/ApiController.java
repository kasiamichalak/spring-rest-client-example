package pl.casmic.springrestclientexample.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.casmic.api.domain.Customer;
import pl.casmic.springrestclientexample.exceptions.NotFoundException;
import pl.casmic.springrestclientexample.services.ApiService;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller
public class ApiController {

    private ApiService apiService;

    public ApiController(ApiService apiService) {
        this.apiService = apiService;
    }

    @GetMapping({"", "/", "/index"})
    public String getIndex() {
        return "index";
    }

    @GetMapping("/customers")
    public String displayAllCustomersList(Model model) {
        model.addAttribute("customers", apiService.getCustomers());
        return "customerlist";
    }

//    @GetMapping("/customers")
//    public String displayCustomerById(Model model, ) {
//        model.addAttribute("customer", apiService.getCustomerByID(id));
//        return "customerlist";
//    }

    @PostMapping("/customers/{id}")
    public String formPost(Model model, @RequestParam("id") String id) throws NotFoundException {

        log.debug("Received customer id value: " + id);

        try {
            Customer customerByID = apiService.getCustomerByID(id);
            List<Customer> list = new ArrayList<>();
            list.add(customerByID);
            model.addAttribute("customers", list);
            return "customerlist";
        } catch (Exception exception) {
            throw new NotFoundException("There is no customer with id " + id);
        }
    }

    //nie działa wyświetlanie własnej strony 404error
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    public ModelAndView handleNotFoundException(Exception exception) {

        log.error("Handling not found exception");
        log.error(exception.getMessage());

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("404error");
        modelAndView.addObject("exception", exception);

        return modelAndView;
    }
}
