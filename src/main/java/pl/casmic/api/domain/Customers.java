
package pl.casmic.api.domain;

import java.util.List;

public class Customers {

    private Meta meta;
    private List<Customer> customers = null;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

}
